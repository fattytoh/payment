@include('includes.header')
<!-- <div class="row">
    <div class="col-lg-2 col-md-12 col-6 mb-4">
        <div class="card">
            <div class="card-body">
                <span class="d-block fw-semibold mb-1">Total Donate</span>
                <h3 class="card-title text-nowrap mb-1"></h3>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6 col-lg-4 col-xl-4 order-0 mb-4">
        <div class="card h-100">
            <div class="card-header d-flex align-items-center justify-content-between pb-0">
                <div class="card-title">
                    <h5 class="m-0 me-2">Top 10 Donation</h5>
                </div>
            </div>
            <div class="card-body">
                <div class="d-flex justify-content-between align-items-center mb-3" style="position: relative;">
                    <ul class="p-0 m-0 w-100">
                    @foreach($donation_list as $key => $li_data)
                        <li class="d-flex mb-4 pb-1">
                            <div class="avatar flex-shrink-0 me-3">
                                <span class="avatar-initial rounded bg-label-primary">{{ $key + 1 }}</span>
                            </div>
                            <div class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                                <div class="me-2">
                                    <h6 class="mb-0">{{ $li_data['campaign'] }}</h6>
                                </div>
                                <div class="user-progress">
                                    <small class="fw-semibold">{{ $li_data['total'] }}</small>
                                </div>
                            </div>
                        </li>
                    @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div> -->
@include('includes.footer')