@include('includes.header')
<div class="card">
<div class="card-body">
    <h1>Donation Detail</h1>
    <div class="text-right mb-3">
        <a href="{{ route('adminDonationList') }}" class="btn btn-primary">Back</a>
    </div>
    <div class="form-group mb-3">
        <label>Donator Name</label>
        <span class="form-control">{{$donationinfo['name']}}</span>
    </div>
    <div class="form-group mb-3">
        <label>Donator Mobile</label>
        <span class="form-control">{{$donationinfo['mobile']}}</span>
    </div>
    <div class="form-group mb-3">
        <label>Campaign</label>
        <span class="form-control">{{$donationinfo['campaign_name']}}</span>
    </div>
    <div class="form-group mb-3">
        <label>Donation Date</label>
        <span class="form-control">{{date("Y-m-d H:i:s", strtotime($donationinfo['created_at']))}}</span>
    </div>
    <div class="form-group mb-3">
        <label>Payment Method</label>
        <span class="form-control">{{$donationinfo['dis_method'][$donationinfo['method']]}}</span>
    </div>
    <div class="form-group mb-3">
        <label>Amount</label>
        <span class="form-control">{{$donationinfo['amount']}}</span>
    </div>
    <div class="form-group mb-3">
        <label>Status</label>
        <span class="form-control">{{$donationinfo['status']}}</span>
    </div>
    <div class="form-group mb-3">
        <label>Payment Reference</label>
        <span class="form-control">{{$donationinfo['payment_ref']}}</span>
    </div>
    <!-- <div class="form-group mb-3">
        <label>Status</label>
        <span class="form-control" style="min-height:200px;">{{$donationinfo['status']}}</span>
    </div> -->
</div>
</div>
@include('includes.footer')