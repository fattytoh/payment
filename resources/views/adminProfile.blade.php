@include('admin.includes.header')
<div class="card">
<div class="card-body">
    <h1>Update Profile</h1>
    @if (session()->has('success_msg'))
    <div class="alert alert-success" role="alert">
        {{ session()->get('success_msg') }}
    </div>
    @endif
    <form id="AdminForm" action="{{ route('adminProfileUpdate') }}" method="post" enctype="multipart/form-data">
    @csrf
        <div class="form-group mb-3">
            <label for="username">Username</label>
            <span type="text" class="form-control" >{{ auth()->user()->username }}</span>
        </div>
        <div class="form-group mb-3">
            <label for="old_password">Old Password</label>
            <input type="password" class="form-control" id="old_password" name="old_password" placeholder="Old Password" value="">
            @if ($errors->has('old_password'))
            <span class="text-danger mt-2 pl-2">{{ $errors->first('old_password') }}</span>
            @endif
        </div>
        <div class="form-group mb-3">
            <label for="password">New Password</label>
            <input type="password" class="form-control" id="password" name="password" placeholder="New Password" value="">
            @if ($errors->has('password'))
            <span class="text-danger mt-2 pl-2">{{ $errors->first('password') }}</span>
            @endif
        </div>
        <div class="form-group mb-3">
            <label for="cfm_password">Confirm New Password</label>
            <input type="password" class="form-control" id="cfm_password" name="cfm_password" placeholder="Confirm Password" value="">
            @if ($errors->has('cfm_password'))
            <span class="text-danger mt-2 pl-2">{{ $errors->first('cfm_password') }}</span>
            @endif
        </div>
        <button type="submit" class="btn btn-success">Update Profile</button>
    </form>
</div>
</div>
@include('admin.includes.footer')