@include('includes.loginheader')
<div class="container">
    <div class="d-flex flex-wrap justify-content-center">
        <div class="checkout-box">
            <h1 class="text-center">Donation</h1>
            <div class="row">
                <div class="form-group">
                    <label for="email">Campaign</label>
                    <div class="form-control">{{ $campaign }}</div>
                </div>
                <div class="form-group">
                    <label for="email">Name</label>
                    <div class="form-control">{{ $name }}</div>
                </div>
                <div class="form-group">
                    <label for="email">Mobile</label>
                    <div class="form-control">{{ $mobile }}</div>
                </div>
                <div class="form-group">
                    <label for="email">Amount</label>
                    <div class="form-control">{{ $amount }}</div>
                </div>
                <form id="checkout_form">
                    <div id="checkout" class="mb-3">
                    </div> 
                    <button class="btn btn-block btn-success">Pay Now</button>
                    <small id="checkout_error" class="text-danger"></small>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="https://js.stripe.com/v3/"></script>
<script>
    const stripe = Stripe('{{ env('STRIPE_KEY') }}');
    const elements = stripe.elements({
        clientSecret: '{{ $client_secret }}'
    });
    const paymentElement = elements.create('payment');
    paymentElement.mount("#checkout");
    const form = document.getElementById('checkout_form');

    form.addEventListener('submit', async (event) => {
        event.preventDefault();

        const {error} = await stripe.confirmPayment({
            //`Elements` instance that was used to create the Payment Element
            elements,
            confirmParams: {
                return_url: '{{ route('stripeHandle', ['id' => $id ]) }}',
            },
        });

        if (error) {
            // This point will only be reached if there is an immediate error when
            // confirming the payment. Show error to your customer (for example, payment
            // details incomplete)
            const messageContainer = document.querySelector('#checkout_error');
            messageContainer.textContent = error.message;
        } else {
            // Your customer will be redirected to your `return_url`. For some payment
            // methods like iDEAL, your customer will be redirected to an intermediate
            // site first to authorize the payment, then redirected to the `return_url`.
        }
    });
</script>
@include('includes.loginfooter')