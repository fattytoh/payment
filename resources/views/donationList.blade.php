@include('includes.header')
<div class="card">
<div class="card-body">
    <h1>Donation List</h1>
    <form id="transForm" action="{{ route('adminDonationList') }}" method="get">
        <div class="d-flex flex-wrap justify-content-end align-items-center">
            <label class="col-form-label mr-2">From</label>
            <input class="form-control mr-2" type="date" value="{{ $start_date }}" id="start_date" name="start_date" style="max-width:150px;">
            <label class="col-form-label mr-2">To</label>
            <input class="form-control mr-2" type="date" value="{{ $end_date }}" id="end_date" name="end_date" style="max-width:150px;">
            <button type="submit" class="btn btn-success">Filter</button>
        </div>
    </form>
    <table id="maintable" class="table petlist-table">
        <thead>
            <tr>
                <th>No</th>
                <th>Date</th>
                <th>Full Name</th>
                <th>Method</th>
                <th>Amount</th>
                <th>Description</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($list_data as $key => $li)
                <tr>
                    <td>{{ ($key + 1) }}</td>
                    <td>{{ date('Y-m-d', strtotime($li->created_at)) }}</td>
                    <td>{{ $li->name }}</td>
                    <td>{{ $dis_method[$li->method] }}</td>
                    <td>{{ $li->amount }}</td>
                    <td>{{ $li->campaign_name }}</td>
                    @if($li->status == 'SUCCESS')
                    <td><div class="bg-success high-box">{{ $li->status }}</div></td>
                    @elseif($li->status == 'FAILED')
                    <td><div class="bg-danger high-box">{{ $li->status }}</div></td>
                    @else
                    <td><div class="bg-warning high-box">{{ $li->status }}</div></td>
                    @endif
                    <td>
                        <a href="{{ route('adminDonationInfo', ['id' =>  $li->id]) }}" class="btn btn-success mr-2">VIEW DETAILS</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
</div>
@include('includes.footer')