@include('includes.loginheader')
<div class="thank-container">
    <div>
        <h1 class="text-center mb-0">SORRY YOUR PAYMENY IS NOT COMPLETE</h1>
        <h1 class="text-center mb-0">PLEASE CONTACT ADMIN</h1>
        <h1 class="text-center mb-0">REDIRECTING BACK TO </h1>
        <h3 class="text-center mb-0">PERINTISBARAKAH.COM.MY IN </h3>
        <h3 class="text-center"><span id="timer">10</span> SECONDS</h3>
    </div>
</div>
<script>
    $(document).ready(function(){
        var count = 10;
        var countdown = setInterval(function(){
            $("#timer").html(count);
            if (count == 0) {
                clearInterval(countdown);
                window.open('https://perintisbarakah.com.my/', "_self");
            }
            count--;
        }, 1000);
    })
</script>
@include('includes.loginfooter')