<aside id="layout-menu" class="layout-menu menu-vertical menu bg-menu-theme" data-bg-class="bg-menu-theme" style="touch-action: none; user-select: none; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
    <div class="app-brand demo">
        <a href="{{ route('home') }}" class="app-brand-link">
            <span class="app-brand-logo demo">
            <i class="fa-solid fa-p menu-icon"></i>
            </span>
            <span class="app-brand-text demo menu-text fw-bold ms-2">Payment</span>
        </a>
        <a href="javascript:void(0);" class="layout-menu-toggle menu-link text-large ms-auto">
            <i class="bx bx-chevron-left bx-sm align-middle"></i>
        </a>
    </div>
    <div class="menu-inner-shadow"></div>
    <ul class="menu-inner py-1 ps ps--active-y">
        <li class="menu-item">
            <a href="{{ route('home') }}" class="menu-link">
                <i class="fa-solid fa-house menu-icon"></i>
                <div>Dashboard</div>
            </a>
        </li>
        <li class="menu-item">
            <a href="{{ route('adminDonationList') }}" class="menu-link">
            <i class="fa-solid fa-circle-dollar-to-slot menu-icon"></i>
                <div>Donation Report</div>
            </a>
        </li>
    </ul>
</aside>