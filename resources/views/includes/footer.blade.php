                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="{{ asset('assets/js/bootstrap.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/apexcharts.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/hammer.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/menu.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/datatables-bootstrap5.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/popper.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/perfect-scrollbar.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/typeahead.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/admin.js') }}"></script>
        <script>
             $(document).ready(function(){
                @if (session()->has('sweet_error_msg'))
                    Swal.fire({
                        icon: 'error',
                        title: "{{ session()->get('sweet_error_title') }}",
                        text: "{{ session()->get('sweet_error_msg') }}"
                    })
                @endif
                @if (session()->has('sweet_success_msg'))
                    Swal.fire({
                        icon: 'success',
                        title: "{{ session()->get('sweet_success_title') }}",
                        text: "{{ session()->get('sweet_success_msg') }}"
                    })
                @endif
            })
        </script>
    </body>
</html>