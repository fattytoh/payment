@include('includes.loginheader')
<div class="authentication-wrapper authentication-basic container-p-y">
    <div class="authentication-inner">
        <div class="card">
            <div class="card-body">
                <div class="app-brand justify-content-center">
                    <a href="" class="app-brand-link gap-2">Payment admin</a>
                </div>
                <h4 class="mb-4">Welcome to Payment! 👋</h4>
                <form id="login-form" class="mb-3 fv-plugins-bootstrap5 fv-plugins-framework" action="{{ route('adminlogin') }}" method="post">
                @csrf
                    <div class="mb-3 fv-plugins-icon-container">
                        <label for="email" class="form-label">Username</label>
                        <input type="text" class="form-control" id="username" name="username" placeholder="Username" autofocus="">
                        <div class="fv-plugins-message-container invalid-feedback">
                            @if ($errors->has('username'))
                                {{ $errors->first('username') }}
                            @endif
                        </div>
                    </div>
                    <div class="mb-3 fv-plugins-icon-container">
                        <label for="email" class="form-label">Password</label>
                        <input type="password" class="form-control" id="password" name="password" placeholder="Password" autofocus="">
                        <div class="fv-plugins-message-container invalid-feedback">
                            @if ($errors->has('password'))
                                {{ $errors->first('password') }}
                            @endif
                        </div>
                    </div>
                    <div class="mb-3">
                        <button class="btn btn-primary d-grid w-100" type="submit">Sign in</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@include('includes.loginfooter')