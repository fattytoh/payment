<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::controller(PaymentController::class)->group(function () {
    Route::post('/createPayment', 'createPayment')->name('paymentCreate');
    Route::get('/test', 'testing')->name('tester');
});

Route::prefix('callback')->controller(PaymentController::class)->group(function () {
    Route::post('/billplz', 'BillplzCallback')->name('billplzcallback');
    Route::any('/stripe/webhook', 'stripeWebhook')->name('stripeWebhook');
});
