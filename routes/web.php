<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
Route::prefix('paymentadmin')->controller(UserController::class)->group(function () {
    Route::post('/login', 'Login')->name('adminlogin');
    Route::get('/logout', 'Logout')->name('adminlogout');
    Route::get('/usrlogin', 'LoginPage')->name('login');
});


// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/thankyou', function () {
    return view('thankyou');
})->name('thanksyou');
Route::get('/payment/fails', function () {
    return view('fails');
})->name('paymentfails');

Route::controller(PaymentController::class)->group(function () {   
    Route::get('/stripe/payment/{id}', 'stripeCheckout')->name('stripeCheckout');
});

Route::prefix('paymentadmin')->middleware('auth')->controller(UserController::class)->group(function () {   
    Route::get('/', 'index')->name('home');
    Route::post('/profile/update', 'updateAdminProfile')->name('adminProfileUpdate');
    Route::get('/profile', 'AdminProfile')->name('adminProfile');
    Route::get('/donationReport', 'DonationList')->name('adminDonationList');
    Route::get('/donation/{id}', 'DonationInfo')->name('adminDonationInfo');
});

Route::prefix('callback')->controller(PaymentController::class)->group(function () {
    Route::get('/stripe/{id}', 'stripeHandle')->name('stripeHandle');
    Route::any('/paypal/success/{id}', 'paypalSuccess')->name('paypalSuccess');
    Route::any('/paypal/fail/{id}', 'paypalFails')->name('paypalFails');
});
