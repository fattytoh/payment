<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Payment;
use App\Models\Callback;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Http;

class PaymentController extends Controller
{
    //
    public $billplz_live_link = 'https://www.billplz.com/api/';
    public $billplz_test_link = 'https://www.billplz-sandbox.com/api/';
    public $paypal_live_link = 'https://api-m.paypal.com/';
    public $paypal_test_link = 'https://api-m.sandbox.paypal.com/';

    public function stripePayment(Request $request){
        if(env('APP_ENV') == 'local'){
            $stripe_secret = env('STRIPE_TEST_SECRET');
        }
        else{
            $stripe_secret = env('STRIPE_SECRET');
        }
        $stripe = new \Stripe\StripeClient($stripe_secret);
        $amount = filter_var($request->input('amount'), FILTER_SANITIZE_NUMBER_INT);
        $res = $stripe->paymentIntents->create([
            'amount' => $amount,
            'currency' => 'myr',
            'automatic_payment_methods' => [
              'enabled' => true,
            ],
        ]);
        
        return [
            'ref' => $res->id,
            'secret' => $res->client_secret,
        ];
    }

    //
    public function payPalPayment(Request $request, $payment_id){
        if(env('APP_ENV') == 'local'){
            $base_link =  $this->paypal_test_link;
            $client_id = env('PAYPAL_TEST_CLIENT_ID');
            $client_secret = env('PAYPAL_TEST_CLIENT_SECRET');
        }
        else{
            $base_link =  $this->paypal_live_link;
            $client_id = env('PAYPAL_CLIENT_ID');
            $client_secret = env('PAYPAL_CLIENT_SECRET');
        }

        $response = Http::accept('application/json')
        ->withBasicAuth($client_id , $client_secret)
        ->post( $base_link.'v2/checkout/orders',[
            'intent' => 'CAPTURE',
            'purchase_units' => [
                [
                    "amount" => [
                        "currency_code" => 'MYR',
                        "value" => $request->input('amount')
                    ]
                ]
            ],
            'payment_source' => [
                "paypal" => [
                    "experience_context" => [
                        "payment_method_preference" => "IMMEDIATE_PAYMENT_REQUIRED",
                        "shipping_preference"=>'NO_SHIPPING',
                        "user_action"=>'PAY_NOW',
                        "landing_page"=> "NO_PREFERENCE",
                        "return_url"=> route('paypalSuccess', ['id' => $payment_id]),
                        "cancel_url"=> route('paypalFails', ['id' => $payment_id]),
                    ]
                ]
            ]
        ]);

        $data =  $response->json();

        $links = '';
        foreach($data['links'] as $li){
            if($li['rel'] == 'payer-action'){
                $links = $li['href'];
                break;
            }
        }
        return [
            'ref' => $data['id'],
            'link' => $links
        ];
    }

    //
    public function BillplzPayment(Request $request){
        if(env('APP_ENV') == 'local'){
            $base_link =  $this->billplz_test_link;
            $auth_key = env('BILLPLZ_TEST_KEY');
            $bill_code = env('BILLPLZ_COLLECTION_TEST_KEY');
        }
        else{
            $base_link =  $this->billplz_live_link;
            $auth_key = env('BILLPLZ_KEY');
            $bill_code = env('BILLPLZ_COLLECTION_KEY');
        }

        $response = Http::withBasicAuth($auth_key , '')
        ->post( $base_link.'v3/bills', [
            'collection_id' =>  $bill_code,
            'description' => $request->input('campaign'),
            'email' => $request->input('email'),
            'name' => $request->input('name'),
            'amount' => filter_var($request->input('amount'), FILTER_SANITIZE_NUMBER_INT),
            'callback_url' => route('billplzcallback'),
            'redirect_url' => route('thanksyou'),
        ]);

        $data =  $response->json();
        return [
            'ref' => $data['id'],
            'link' => $data['url']
        ];
    }

    public function BillplzCallback(Request $request){
        $payment = Payment::where('method', 'billplz')->where('payment_ref', $request->input('id'))->where('status', 'PENDING')->first();
        if($payment){
            if($request->input('paid') == 'true'){
                $payment->status = 'SUCCESS';
                $payment->payment_record = json_encode($request->all());
                $payment->save();
            }
            else{
                $payment->status = 'FAILED';
                $payment->payment_record = json_encode($request->all());
                $payment->save();
                return redirect()->route('paymentfails');
            }
        }
        return redirect()->route('thanksyou');
    }

    public function createPayment(Request $request){
        $rules = [
            'method'    => 'required|in:billplz,stripe,paypal',
            'name'    => 'required|min:5',
            'mobile'    => 'required',
            'address'    => 'required',
            'email'    => 'required|email',
            'campaign'    => 'required',
            'amount'    => 'required|numeric|gt:0',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json([
                'status' => 0,
                'error' => $validator->errors()->toArray(),
            ]);
        }

        $pay = new Payment();
        $pay->campaign_name = $request->input('campaign');
        $pay->method = $request->input('method');
        $pay->mobile = $request->input('mobile');
        $pay->name = $request->input('name');
        $pay->amount = $request->input('amount');
        $pay->address = $request->input('address');
        $pay->email = $request->input('email');
        if($request->input('method') == 'billplz'){
            $bill_ref = $this->BillplzPayment($request);
            $pay->payment_ref = $bill_ref['ref'];
            $pay->save();
        }
        elseif($request->input('method') == 'stripe'){
            $bill_ref = $this->stripePayment($request);
            $pay->stripe_secret = $bill_ref['secret'];
            $pay->payment_ref = $bill_ref['ref'];
            $pay->save();
            $bill_ref['link'] = route('stripeCheckout', ['id' => $pay->id]);
        }
        elseif($request->input('method') == 'paypal'){
            $pay->payment_ref = '-';
            $pay->save();
            $bill_ref = $this->payPalPayment($request, $pay->id);
            $pay->payment_ref = $bill_ref['ref'];
            $pay->save();
        }

        return response()->json([
            'status' => 1,
            'link' => $bill_ref['link']
        ]);
    }

    public function stripeCheckout(Request $request){
        $checkout = Payment::where('method', 'stripe')->where('id', $request->route('id'))->where('status', 'PENDING')->first();
        if($checkout){
            // stripecheckout
            $data = [
                'id' =>  $request->route('id'),
                'campaign' =>  $checkout->campaign_name,
                'name' =>  $checkout->name,
                'mobile' =>  $checkout->mobile,
                'amount' =>  $checkout->amount,
                'client_secret' =>  $checkout->stripe_secret,
            ];
            return view('stripecheckout',$data);
        }
        else{
            return redirect()->route('thanksyou');
        }
    }

    public function stripeHandle(Request $request){
        if(env('APP_ENV') == 'local'){
            $stripe_secret = env('STRIPE_TEST_SECRET');
        }
        else{
            $stripe_secret = env('STRIPE_SECRET');
        }
        $payment = Payment::where('method', 'stripe')->where('id', $request->route('id'))->where('status', 'PENDING')->first();
        if($payment ){
            $stripe = new \Stripe\StripeClient($stripe_secret);
            $res = $stripe->paymentIntents->retrieve($payment->payment_ref);
            if($res->status == 'requires_payment_method'){
                return redirect()->route('stripeCheckout', ['id' => $request->route('id')]);
            }
            elseif($res->status == 'succeeded'){
                $payment->status = 'SUCCESS';
                $payment->payment_record = json_encode($res);
                $payment->save();
                return redirect()->route('thanksyou');
            }
            else{
                $payment->status = 'FAILED';
                $payment->payment_record = json_encode($res);
                $payment->save();
                return redirect()->route('fails');
            }
        }
        return redirect()->route('thanksyou');
    }

   

    public function stripeWebhook(Request $request){
        if(env('APP_ENV') == 'local'){
            $endpoint_secret = env('STRIPE_TEST_WEBHOOK_KEY');
        }
        else{
            $endpoint_secret = env('STRIPE_WEBHOOK_KEY');
        }
        $payload = @file_get_contents('php://input');
        $sig_header = $_SERVER['HTTP_STRIPE_SIGNATURE'];
        $event = null;

        try
        {
            $event = \Stripe\Webhook::constructEvent($payload, $sig_header, $endpoint_secret);
        } 
        catch(\UnexpectedValueException $e)
        {
                // Invalid payload
                return response()->json([
                    'message' => 'Invalid payload',
                ], 200);
        }
        catch(\Stripe\Exception\SignatureVerificationException $e)
        {
            // Invalid signature
            return response()->json([
                'message' => 'Invalid signature',
            ], 200);
        }

        $callback = new Callback();
        $callback->method = 'stripe';
        $callback->callback_data = json_encode($event->data->object);
        $callback->save();

        if ($event->type == "payment_intent.succeeded")
        {
            $intent = $event->data->object;
            $checkout = Payment::where('method', 'stripe')->where('payment_ref', $intent->id)->where('status', 'PENDING')->first();
            if($checkout ){
                $checkout->status = "SUCCESS";
                $checkout->payment_record = json_encode($intent);
                $checkout->save();
            }

            return response()->json([
                'intentId' => $intent->id,
                'message' => 'Payment succeded'
            ], 200); 
        } 
        elseif ($event->type == "payment_intent.payment_failed")
        {
            //Payment failed to be completed
            
            $intent = $event->data->object;
            $error_message = $intent->last_payment_error ? $intent->last_payment_error->message : "";

            $checkout = Payment::where('method', 'stripe')->where('payment_ref', $intent->id)->where('status', 'PENDING')->first();
            if($checkout ){
                $checkout->status = "FAILED";
                $checkout->payment_record = json_encode($intent);
                $checkout->save();
            }

            return response()->json([
                'intentId' => $intent->id,
                'message' => 'Payment failed: '.$error_message
            ], 400); 
        }

    }

    public function paypalSuccess(Request $request){
        $payment = Payment::where('method', 'paypal')->where('id', $request->route('id'))->where('status', 'PENDING')->first();
        if($payment){
            $payment->status = 'SUCCESS';
            $payment->payment_record = json_encode($request->all());
            $payment->save();
        }
        return redirect()->route('thanksyou');
    }

    public function paypalFails(Request $request){
        $payment = Payment::where('method', 'paypal')->where('id', $request->route('id'))->where('status', 'PENDING')->first();
        if($payment){
            $payment->status = 'FAILED';
            $payment->payment_record = json_encode($request->all());
            $payment->save();
        }

        return redirect()->route('paymentfails');
    }

    public function testing(){
        return redirect()->route('thanksyou');
    }
}