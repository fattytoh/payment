<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Payment;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    //
    public function Login(Request $request){
        $rules = [
            'username'    => 'required',
            'password'    => 'required',
        ];
        $msg = [
            'username.required' => 'Username require',
            'password.required' => 'Password require',
        ];
        $request->validate($rules, $msg);
        $loginusr = User::where('username', $request->input('username'))->where('status','ACTIVE')->first();
        if($loginusr){
            if(!Auth::attempt($request->only('username', 'password'))){ 
                return redirect()->route('adminloginui')->withErrors(['username' => 'Invalid Username', 'password' => 'Invalid Password']);
            }
        }
        else{
            return redirect()->route('adminloginui')->withErrors(['username' => 'Invalid Username1', 'password' => 'Invalid Password1']);
        }

        return redirect()->route('home');
    }

    public function Logout(){
        Auth::logout();
        return redirect()->route('login');
    }

    public function LoginPage(){
        Auth::logout();
        return view('login');
    }

    public function index(){
        $admin_data = [
            'donation_list' => [],
        ];
        return view('dashboard', $admin_data);
    }

    public function updateAdminProfile(){
        $rules = [
            'password'    => 'nullable|min:5',
            'cfm_password'    => 'same:password',
        ];

        $msg = [
        ];

        $request->validate($rules, $msg);
        $errors_input = [];

        if($request->input('password')){
            if(!Hash::check($request->input('old_password'), Auth::user()->password) ){
                $errors_input['old_password'] = "Invalid Old Password";
            }
        }

        if(count( $errors_input) > 0){
            return redirect()->back()->withInput()->withErrors($errors_input);
        }

        $exist_admin = User::find(Auth::user()->id);
        if($request->input('password')){
            $exist_admin->password = Hash::make($request->input('password'));
        }
        $exist_admin->save();

        return redirect()->back()->withInput()->with('success_msg', 'Upadate Admin Succesfully');
    }

    public function AdminProfile(){
        return view('adminProfile');
    }

    public function DonationList(Request $request){
        $sdate = "";
        $edate = "";

        if($request->input("start_date") && $request->input("end_date") ){
            if(strtotime($request->input("start_date")) > strtotime($request->input("end_date"))){
                $sdate = $request->input("end_date");
                $edate = $request->input("start_date");
            }
            else{
                $sdate = $request->input("start_date");
                $edate = $request->input("end_date");
            }

            $list['list_data']  = DB::table('payment')
            ->whereRaw("DATE_FORMAT(created_at, '%Y-%m-%d')>=?",[$sdate])
            ->whereRaw("DATE_FORMAT(created_at, '%Y-%m-%d')<=?",[$edate])
            ->get()->toArray();
            
        }
        elseif($request->input("start_date") && !$request->input("end_date")){
            $sdate = $request->input("start_date");
            $list['list_data']  = DB::table('payment')
            ->whereRaw("DATE_FORMAT(created_at, '%Y-%m-%d')>=?",[$sdate])
            ->get()->toArray();
        }
        elseif(!$request->input("start_date") && $request->input("end_date")){
            $edate = $request->input("end_date");
            $list['list_data']  = DB::table('payment')
            ->whereRaw("DATE_FORMAT(created_at, '%Y-%m-%d')<=?",[$edate])
            ->get()->toArray();
        }
        else{
            $list['list_data']  = DB::table('payment')
            ->get()->toArray();
        }
        
        $config = new ConfigController();
        $list['dis_method'] = $config->getTransactionMethod();
        $list['start_date'] = $sdate;
        $list['end_date'] = $edate;

        return view('donationList', $list);
    }

    public function DonationInfo(Request $request){
        if($request->route('id')){
            $data['donationinfo'] = Payment::where("id", $request->route('id'))->first()->toArray();
            if($data['donationinfo']){
                $config = new ConfigController();
                $data['donationinfo']['dis_method'] = $config->getTransactionMethod();
                return view('donationDetails', $data);
            }
            else{
                return redirect()->route('adminDonationList');
            }
        }
        else{
            return redirect()->route('adminDonationList');
        }
    }
}

