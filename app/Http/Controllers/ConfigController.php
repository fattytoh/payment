<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Payment;
use App\Models\User;

class ConfigController extends Controller
{

    public function getTransactionMethod(){
        return [
            'paypal' => 'Pay Pal',
            'stripe' => 'Stripe',
            'billplz' => 'Billplz',
        ];
    }
}
